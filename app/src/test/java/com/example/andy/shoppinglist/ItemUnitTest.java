package com.example.andy.shoppinglist;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for Item class.
 */
public class ItemUnitTest {
    private final static double DELTA = 1e-10;

    private Item item;

    @Before
    public void initialize() {
        item = new Item();
    }

    @Test()
    public void Item_DefaultConstructor_Correct() {
        assertEquals(Item.DEFAULT_QUANTITY, item.getQuantity(), DELTA);
        assertEquals(Item.DEFAULT_UNIT, item.getUnit());
        assertEquals(Item.DEFAULT_NAME, item.getName());
        assertEquals(Item.DEFAULT_CHECKED, item.isChecked());
    }

    @Test()
    public void Item_AllParams_Correct() {
        int quantity = 3;
        String unit = "unit";
        String name = "name";

        item = new Item(name, quantity, unit);

        assertEquals(quantity, item.getQuantity(), DELTA);
        assertEquals(unit, item.getUnit());
        assertEquals(name, item.getName());
        assertEquals(Item.DEFAULT_CHECKED, item.isChecked());
    }

    @Test()
    public void Item_Clone_Correct() {
        int quantity = 4;
        String unit = "clone unit";
        String name = "clone name";
        Item oldItem = new Item(name, quantity, unit);

        item = new Item(oldItem);

        assertEquals(oldItem.getQuantity(), item.getQuantity(), DELTA);
        assertEquals(oldItem.getUnit(), item.getUnit());
        assertEquals(oldItem.getName(), item.getName());
        assertEquals(oldItem.isChecked(), item.isChecked());
    }

    @Test
    public void SetQuantity_ValidValue_Correct() {
        double quantity = 2.0;
        item.setQuantity(quantity);
        assertEquals(quantity, item.getQuantity(), DELTA);
    }

    @Test()
    public void SetQuantity_LessThanMinimumValue_QuantityResetToMin() {
        item.setQuantity(Item.MINIMUM_QUANTITY - 1.0);
        assertEquals(Item.MINIMUM_QUANTITY, item.getQuantity(), DELTA);
    }

    @Test()
    public void SetUnit_NullValue_NameSetToDefault() {
        item.setUnit(null);
        assertEquals(Item.DEFAULT_UNIT, item.getUnit());
    }

    @Test()
    public void SetUnit_ValidValue_Correct() {
        String unit = "unit";
        item.setUnit(unit);
        assertEquals(unit, item.getUnit());
    }

    @Test()
    public void SetName_NullValue_NameSetToDefault() {
        item.setName(null);
        assertEquals(Item.DEFAULT_NAME, item.getName());
    }

    @Test()
    public void SetName_ValidValue_Correct() {
        String name = "test";
        item.setName(name);
        assertEquals(name, item.getName());
    }

    @Test()
    public void IsChecked_NotChecked_IsFalse() {
        item.setChecked(false);
        assertFalse(item.isChecked());
    }

    @Test()
    public void IsChecked_Checked_IsTrue() {
        item.setChecked(true);
        assertTrue(item.isChecked());
    }
}
