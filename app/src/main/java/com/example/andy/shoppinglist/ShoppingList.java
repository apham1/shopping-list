package com.example.andy.shoppinglist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class ShoppingList extends AppCompatActivity {
    ArrayList<Item> items = new ArrayList<>();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private void initialize() {
        items.add(new Item("item1", 1, ""));
        items.add(new Item("item2", 1, "oz"));
        items.add(new Item("item3", 1, "g"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);
        initialize();

        mRecyclerView = findViewById(R.id.rvItems);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ItemAdapter(items);
        mRecyclerView.setAdapter(mAdapter);
    }
}
