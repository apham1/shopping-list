package com.example.andy.shoppinglist;

public class Item {

    final static double DEFAULT_QUANTITY = 1.0;
    final static double MINIMUM_QUANTITY = 0.0;

    final static String DEFAULT_UNIT = "";
    final static String DEFAULT_NAME = "";
    final static boolean DEFAULT_CHECKED = false;

    private double quantity;
    private String unit;
    private String name;
    private boolean checked;

    public Item() {
        this.quantity = DEFAULT_QUANTITY;
        this.unit = DEFAULT_UNIT;
        this.name = DEFAULT_NAME;
        this.checked = DEFAULT_CHECKED;
    }

    public Item(String name, double quantity, String unit) {
        setQuantity(quantity);
        setUnit(unit);
        setName(name);
        setChecked(DEFAULT_CHECKED);
    }

    public Item(Item item) {
        setName(item.getName());
        setUnit(item.getUnit());
        setQuantity(item.getQuantity());
        setChecked(item.isChecked());
    }

    public void setQuantity(double quantity) {
        if (quantity < MINIMUM_QUANTITY) {
            quantity = MINIMUM_QUANTITY;
        }
        this.quantity = quantity;
    }

    public double getQuantity() {
        return this.quantity;
    }

    public void setUnit(String unit) {
        if (unit == null) {
            this.unit = DEFAULT_UNIT;
        } else {
            this.unit = unit;
        }
    }

    public String getUnit() {
        return this.unit;
    }

    public void setName(String name) {
        if (name == null) {
            this.name = DEFAULT_NAME;
        } else {
            this.name = name;
        }
    }

    public String getName() {
        return this.name;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isChecked() {
        return this.checked;
    }

    @Override
    public String toString() {
        String output = "";

        if (this.quantity % 1 == 0) {
            output += (int)this.quantity;
        } else {
            output += this.quantity;
        }

        if (!this.unit.equals("")) {
            output += " " + this.unit;
        }
        output += " " + this.name;
        return output;
    }
}
